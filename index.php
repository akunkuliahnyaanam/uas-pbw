<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Jamu Nusantara - Blog</title>
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
        <!-- User CSS -->
        <link rel="stylesheet" href="css/userstyle.css">
    </head>
    <body>
        <!-- Navigation-->
        <?php include 'main/nav.php' ; ?>
        <!-- Page Header-->
        <header class="masthead" style="background-image: url('assets/img/home.png')" id=top>
            <div class="container position-relative px-4 px-lg-5">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-md-10 col-lg-8 col-xl-7">
                        <div class="site-heading">
                            <h1>Jamu Nusantara</h1>
                            <span class="subheading">Blog semua tentang jamu nusantara</span>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Main Content-->
        <article class="mb-4">
            <div class="container px-4 px-lg-5">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-md-10 col-lg-8 col-xl-7">
                        <p>Jamu adalah istilah yang berasal dari bahasa Jawa, tepatnya pada 16 Masehi. Kata Jamu berasal dari dua kata, yaitu “Djampi” dan “Oesodo” yang memiliki makna obat, doa, dan juga berarti formula yang berbau magis. Jamu pertama kali muncul pada zaman Kerajaan Mataram atau sekitar 1300 tahun yang lalu. Keberadaan jamu sejak zaman dahulu dapat dilihat dari beberapa bukti sejarah seperti relief pada candi Borobudur. Relief Candi Borobudur yang dibuat oleh Kerajaan Hindu-Budha tahun 772 M menggambarkan kebiasaan meracik dan meminum jamu untuk memelihara kesehatan.</p>
                        <p>Bukti sejarah lainnya yaitu penemuan prasasti Madhawapura dari peninggalan kerajaan Hindu-Majapahit yang menyebut adanya profesi ‘tukang meracik jamu’ yang disebut Acaraki. Ditemukannya Lontar Usada di Bali yang ditulis menggunakan bahasa Jawa kuno menceritakan mengenai penggunaan jamu juga menjadi bukti keberadaan jamu sejak zaman dahulu. Menyebarnya konsumsi jamu di masyarakat dipengaruhi banyaknya ahli botani yang mempublikasikan tulisan-tulisan mengenai ragam dan manfaat tanaman untuk pengobatan.</p>
                        <p>Seperti salah satunya, dimana jamu menjadi ikon dari budaya ayng mewakili kehidupan masyarakat Desa Nguter, Sukoharjo, Solo. Masyarakat disana sangat mendukung upaya pelestarian jamu sebagai bentuk kearifan yang dapat Grameds pelajari pada buku Jamu Sebagai Bagian dari Kearifan Lokal Desa Nguter.</p>
                    </div>
                </div>
            </div>
        </article>
        <!-- Footer-->
        <?php include 'main/footer.php' ; ?>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
    </body>
</html>
