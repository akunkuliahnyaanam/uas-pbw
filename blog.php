<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Jamu Nusantara - Blog</title>
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
        <!-- User CSS -->
        <link rel="stylesheet" href="css/userstyle.css">
    </head>
    <body>
        <!-- Navigation-->
        <?php include 'main/nav.php' ; ?>
        <!-- Page Header-->
        <header class="masthead" style="background-image: url('assets/img/blog.png')" id=top>
            <div class="container position-relative px-4 px-lg-5">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-md-10 col-lg-8 col-xl-7">
                        <div class="site-heading">
                            <h1>Blog</h1>
                            <span class="subheading">Semua tentang jamu.</span>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Main Content-->
        <div class="container px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                    <!-- Post preview-->
                    <div class="post-preview">
                        <a href="blog/0post.php">
                            <h2 class="post-title">Manfaat Kunyit Asam: Jamu Daya Tahan hingga Atasi Haid </h2>
                            <h3 class="post-subtitle">Ramuan tradisional yang tingkatkan imun, redakan nyeri haid hingga obati maag.</h3>
                        </a>
                        <p class="post-meta">
                            Posted by
                            <a href="about.php">Khoirul Anam</a>
                            on December 14, 2022 <span class="badge bg-info rounded-pill post-subtitle">New
                        </p>
                    </div>
                    <!-- Divider-->
                    <hr class="my-4" />
                    <!-- Post preview-->
                    <div class="post-preview">
                        <a href="blog/1post.php"><h2 class="post-title">Pedagang jamu Lebak tak tergerus pandemi, bisa untung Rp200 ribu/hari</h2></a>
                        <p class="post-meta">
                            Posted by
                            <a href="about.php">Khoirul Anam</a>
                            on November 18, 2022
                        </p>
                    </div>
                    <!-- Divider-->
                    <hr class="my-4" />
                    <!-- Post preview-->
                    <div class="post-preview">
                        <a href="blog/2post.php">
                            <h2 class="post-title">Cara Membuat Jamu Beras Kencur ala Rumahan yang Menyehatkan</h2>
                            <h3 class="post-subtitle">Jamu Beras Kencur ala Rumahan</h3>
                        </a>
                        <p class="post-meta">
                            Posted by
                            <a href="about.php">Khoirul Anam</a>
                            on November 1, 2022
                        </p>
                    </div>
                    <!-- Divider-->
                    <hr class="my-4" />
                    <!-- Post preview-->
                    <div class="post-preview">
                        <a href="blog/3post.php">
                            <h2 class="post-title">Jamu Gendong Keliling Kini Semakin Langka</h2>
                            <h3 class="post-subtitle">Makin berkembangnya teknologi makin maju pula manusia. Begitu juga dengan para penjual jamu gendong.</h3>
                        </a>
                        <p class="post-meta">
                            Posted by
                            <a href="about.php">Khoirul Anam</a>
                            on August 21, 2022
                        </p>
                    </div>
                    <!-- Divider-->
                    <hr class="my-4" />
                    <!-- Pager-->
                    <div class="d-flex justify-content-end mb-4"><a class="btn btn-primary text-uppercase" href="#top">Older Posts →</a></div>
                </div>
            </div>
        </div>
        <!-- Footer-->
        <?php include 'main/footer.php' ; ?>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
    </body>
</html>
