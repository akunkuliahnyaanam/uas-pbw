<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Jamu Nusantara - Blog</title>
        <link rel="icon" type="../image/x-icon" href="../assets/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="../css/styles.css" rel="stylesheet" />
    </head>
    <body>
        <!-- Navigation-->
        <?php include '../main/nav.php' ; ?>
        <!-- Page Header-->
        <header class="masthead" style="background-image: url('../assets/img/blog/1post.png')">
            <div class="container position-relative px-4 px-lg-5">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-md-10 col-lg-8 col-xl-7">
                        <div class="post-heading">
                            <h1>Pedagang jamu Lebak tak tergerus pandemi, bisa untung Rp200 ribu/hari</h1>
                            <span class="meta">
                                Posted by
                                <a href="#!">Khoirul Anam</a>
                                on November 18, 2022
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Post Content-->
        <article class="mb-4">
            <div class="container px-4 px-lg-5">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-md-10 col-lg-8 col-xl-7">
                        <p>Pedagang jamu gendong di Kabupaten Lebak, Provinsi Banten, dapat meraup keuntungan Rp130 ribu sampai Rp200 ribu per hari, sehingga dapat menumbuhkan ekonomi keluarga.</p>
                        <p>"Kami selama 30 tahun berjualan jamu mampu beli rumah BTN Palaton Rangkasbitung, juga dua putrinya menjadi sarjana," kata Mba Endo (55) saat ditemui di kediamannya di Lebak, Selasa.</p>
                        <p>Selama ini ekonomi keluarga pedagang jamu tidak terdampak pandemi COVID-19, karena mereka kebutuhan pangan relatif terpenuhi.</p>
                        <p>"Kami sebagai perantau dari Jawa ke Lebak kini bisa meraup keuntungan bersih Rp200 ribu/hari dan bisa mengubah nasib menjadi lebih baik dan hidup sejahtera, " kata Endo yang sudah lama ditinggal suami itu.</p>
                        <p>Begitu juga pedagang jamu lainnya Atun (18) mengaku baru tiga pekan terakhir berjualan keliling di wilayah Desa Rangkasbitung mampu meraup keuntungan Rp130 ribu/ hari.</p>
                        <p>Berjalan kaki melintasi hutan dan kawasan perkebunan kelapa sawit, tidak menjadikan halangan.</p>
                        <p>"Kami berangkat pukul 6.00 WIB dan pulang pukul 10.00 WIB dan setiap hari habis, terlebih saat ini adanya pandemi COVID-19 banyak konsumennya, " kata Atun sambil menyatakan ia baru lulus SMK di Solo Jawa Tengah.</p>
                        <p>Perkumpulan Pedagang Jamu Gendong Kabupaten Lebak Parjiem (58) mengaku saat ini jumlah pedagang jamu gendong di daerah ini mencapai 550 orang tersebar di 28 kecamatan.</p>
                        <p>Mereka kebanyakan warga perantau dari sejumlah daerah di Jawa Tengah, namun saat ini sudah banyak menjadi warga Lebak, Banten.</p>
                        <p>Kehadiran pedagang jamu gendong itu, selain menumbuhkan ekonomi keluarga juga ekonomi petani, karena mereka memproduksi sendiri dan tidak mendatangkan dari Jawa Tengah.</p>
                        <p>"Semua bahan baku jamu itu dari tanaman obat-obatan dibeli dari petani hingga Rp500 ribu per bulan," katanya.</p>
                        <p>Sementara itu Kepala Dinas Koperasi dan UKM Kabupaten Lebak Yudawati mengatakan pemerintah daerah mendorong pedagang jamu gendong berkembang untuk menumbuhkan ekonomi keluarga.</p>
                        <p>Kebanyakan pedagang jamu gendong itu, kata dia, memiliki identitas kependudukan Lebak.</p>
                        <p>"Kami di tengah penyebaran Virus Corona juga memberikan bantuan melalui Program Bantuan Produktif Usaha Mikro (BPUM) sebesar Rp2,4 juta per KK," katanya.</p>
                        <p>Penyaluran bantuan modal itu sebanyak 13.600 pelaku unit usaha, termasuk pedagang jamu gendong.</p>
                    </div>
                </div>
            </div>
        </article>
        <!-- Footer-->
        <?php include '../main/footer.php' ; ?>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="../js/scripts.js"></script>
    </body>
</html>
