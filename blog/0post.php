<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Jamu Nusantara - Blog</title>
        <link rel="icon" type="../image/x-icon" href="../assets/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="../css/styles.css" rel="stylesheet" />
    </head>
    <body>
        <!-- Navigation-->
        <?php include '../main/nav.php' ; ?>
        <!-- Page Header-->
        <header class="masthead" style="background-image: url('../assets/img/blog/0post.png')">
            <div class="container position-relative px-4 px-lg-5">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-md-10 col-lg-8 col-xl-7">
                        <div class="post-heading">
                            <h1>Manfaat Kunyit Asam: Jamu Daya Tahan hingga Atasi Haid</h1>
                            <h2 class="subheading">Ramuan tradisional yang tingkatkan imun, redakan nyeri haid hingga obati maag.</h2>
                            <span class="meta">
                                Posted by
                                <a href="#!">Khoirul Anam</a>
                                on December 14, 2022
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Post Content-->
        <article class="mb-4">
            <div class="container px-4 px-lg-5">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-md-10 col-lg-8 col-xl-7">
                        <p>Kunyit asam sejak dulu dikenal sebagai minuman yang memiliki berbagai khasiat bagi kesehatan tubuh. Seperti namanya, jamu ini berbahan utama kunyit atau kunir yang merupakan rempah-rempah yang telah lama dikenal sebagai obat herbal tradisional.</p>
                        <p>Jamu kunyit asam juga dipercaya dapat meningkatkan sistem kekebalan tubuh tanpa efek samping berlebihan. Ini berkat kandungan kurkumin dalam kunyit yang berperan sebagai antioksidan alami dan antiperadangan.</p>
                        <h2 class="section-heading">Kandungan Jamu Kunyit Asam</h2>
                        <p>Bila kamu membuat jamu kunyit asam secara tradisional, maka bahan utamanya adalah kunyit yang dicampur asam Jawa, gula merah, garam dan gula putih. Berikut masing-masing kandungan nutrisi dari kunyit dan asam.</p>
                        <p>Kunyit</p>
                        <ul>
                            <li>Kalori</li>
                            <li>Protein</li>
                            <li>Lemak</li>
                            <li>Karbohidrat</li>
                            <li>Gula</li>
                            <li>Zat besi</li>
                            <li>dll.</li>
                        </ul>
                        <p>Asam Jawa</p>
                        <ul>
                            <li>Magnesium</li>
                            <li>Potasium</li>
                            <li>Vitamin B1</li>
                            <li>Vitamin B2</li>
                            <li>Vitamin B3</li>
                            <li>dll.</li>
                        </ul>
                        <h2 class="section-heading">Manfaat/Khasiat Jamu Kunyit Asam</h2>
                        <p></p>
                        <a href="#!"><img class="img-fluid" src="../assets/img/blog/01post.png" alt="..." /></a>
                        <span class="caption text-muted">Kunyit bisa membantu mengatasi berbagai masalah penyakit serius, mengurangi peradangan hingga nyeri haid.</span>
                        <h2 class="section-heading">1. Mampu Menurunkan Kadar Gula Darah</h2>
                        <p>Buah asam memiliki kandungan magnesium dan polifenol yang mampu mencegah penyakit diabetes, tidak terkecuali kunyit yang mampu menurunkan kadar gula darah. Manfaat ini adalah efek dari sifat antiinflamasi kunyit dan asam, Toppers. Ketika peradangan di tubuh mereda, tubuh dapat merespon hormon insulin dengan lebih baik sehingga glukosa di tubuh bisa diambil menjadi energi.</p>
                        <p>Selain itu, kunyit juga memiliki enzim alpha-amilase yang mampu menghentikan penyerapan karbohidrat. Oleh sebab itu, kunyit asam sering direkomendasikan bagi para pengidap diabetes.</p>
                        <h2 class="section-heading">2. Mencegah dan Mengurangi Risiko Kanker</h2>
                        <p>Kunyit mengandung kurkumin yang memiliki khasiat untuk mencegah penyakit kanker. Kurkumin juga berfungsi sebagai antiperadangan dan antioksidan, sehingga dapat mencegah risiko mutasi sel kanker dalam tubuh.</p>
                        <h2 class="section-heading">3. Meningkatkan Daya Tahan Tubuh</h2>
                        <p>Mengonsumsi kunyit asam secara teratur dapat meningkatkan daya tahan tubuh, karena kunyit asam mengandung anti bakteri dan anti peradangan.</p>
                        <p>Secara tidak langsung, kunyit asam juga dapat meningkatkan kekebalan tubuh melawan penyakit.</p>
                        <h2 class="section-heading">4. Meredakan Nyeri Saat Haid</h2>
                        <p>Kunyit asam sudah sejak lama menjadi salah satu senjata rahasia wanita untuk meredakan nyeri saat haid. Manfaat kunyit asam dapat dirasakan karena kandungan kurkumin dalam kunyit.</p>
                    </div>
                </div>
            </div>
        </article>
        <!-- Footer-->
        <?php include '../main/footer.php' ; ?>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="../js/scripts.js"></script>
    </body>
</html>
