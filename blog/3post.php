<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Jamu Nusantara - Blog</title>
        <link rel="icon" type="../image/x-icon" href="../assets/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="../css/styles.css" rel="stylesheet" />
    </head>
    <body>
        <!-- Navigation-->
        <?php include '../main/nav.php' ; ?>
        <!-- Page Header-->
        <header class="masthead" style="background-image: url('../assets/img/blog/3post.png')">
            <div class="container position-relative px-4 px-lg-5">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-md-10 col-lg-8 col-xl-7">
                        <div class="post-heading">
                            <h1>Jamu Gendong Keliling Kini Semakin Langka</h1>
                            <h2 class="subheading">Makin berkembangnya teknologi makin maju pula manusia. Begitu juga dengan para penjual jamu gendong.</h2>
                            <span class="meta">
                                Posted by
                                <a href="#!">Khoirul Anam</a>
                                on August 21, 2022
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Post Content-->
        <article class="mb-4">
            <div class="container px-4 px-lg-5">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-md-10 col-lg-8 col-xl-7">
                    <p>Dulu, penjual jamu gendong yang membawa cepon (bakul) jamunya dengan sewet (selendang) khas tukang jamu dengan kebaya khas Jawa, dapat mudah ditemui di desa-desa.</p>
                    <p>Namun kini semakin berkurang, bahkan dapat dikatakan sangat susah ditemui atau langka.</p>
                    <p>Inem, salah satu penjual jamu gendong yang tersisa. Ia menuturkan, kawan-kawannya yang sesama penjual jamu gendong sudah banyak yang berhenti atau bahkan sudah tiada.</p>
                    <p>Sehingga, hanya tersisa sedikit yang masih menjaga eksistensi dan ciri khas jamu gendong saat ini.</p>
                    <p>Penjual jamu gandong asal Wonogiri ini menyampaikan bahwa alasan dirinya masih bertahan dengan khas tradisional sendiri dikarenakan ketidakmampuannya untuk menunggangi sepeda.</p>
                    <p>Disisi lain keinginan dirinya untuk menjaga ciri khas penjual jamu yaitu dibawa dengan bakul dan digendong.</p>
                    <p>“Ya jamu gendong namanya, ya jamunya dibakul dan digendong pake sewet (selendang) khas, dengan pakai kebaya. Kalau ga pake kebaya ya bukan Jamu gendong namanya,” bebernya dengan bahasa jawa.</p>
                    <p>Dengan pengalaman berjualan jamu 12 tahun di Kabupaten Indramayu dan di Cirebon kurang lebih 20 tahunan jugalah yang membuat mbah Inem enggan mengubah budaya jamu gendong.</p>
                    <p>Namun, dikarenakan usianya yang sudah renta ia hanya berjualan di kawasan Jalan Buyut Pegambiran, Kutasirap.</p>
                    <p>“Saya cuma berjualan disini, dari pertigaan pronggol sampe bawah flyover yang jualan sate tuh, trus pulang pergi saya naik becak,” katanya kepada FC.</p>
                    <p>Baginya, berjualan jamu gendong bukan hanya untuk mengais rezeki melainkan melestarikan budaya leluhur. Setiap hari Inem keliling menjajakan jamu carican tradisionalnya.</p>
                    <p>Diantaranya terdapat Kunyit asem, beras kencur, anggur, manis, jahe, suruh, dan tentunya air panas dalam termos untuk menyeduh jamu instan.</p>
                    <p>Jamu instan yang dibawa Inem sendiri diantaranya ada buyung upik, pegel linu, dan masih banyak lagi. Pastinya, dengan harga murah yaitu kisaran Rp4.000 untuk setiap jakunya termasuk jamu racik dadakan.</p>
                    <p>“Jamunya harganya Rp4.000an saja, untuk pendapatan ya nggak nentu. Apalagi masa pandemi Covid-19 gini, tapi cukup untuk makan sehari-hari ,” ujarnya sembari tersenyum.</p>
                    <p>Semasa, pandemi Covid-19 seperti ini setidaknya Inem menjual puluhan kantung plastik berukuran 15×4 cm jamu dan puluhan sachet jamu instan lainnya.</p>
                    </div>
                </div>
            </div>
        </article>
        <!-- Footer-->
        <?php include '../main/footer.php' ; ?>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="../js/scripts.js"></script>
    </body>
</html>
